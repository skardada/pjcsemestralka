#pragma once
#include <memory>
#include <vector>
#include <iostream>
class Graph {
public:
	struct Edge {
		int source, dest, weight;
		Edge(int src, int dst,  int wght) :source(src), dest(dst), weight( wght){

		}
	};
private:
	std::vector<Edge> edges;
	int numVertices;
public:
	Graph(std::vector<Edge> ptr, int vertices) : edges{ ptr }, numVertices{vertices} {
	}
	Graph(const Graph &g) {
		numVertices = g.numVertices;
		edges = g.edges;
	}
	//Prints resulting path lengths on given outputstream
	void BellmanFord(std::ostream &os, int source);
};