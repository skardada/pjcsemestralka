﻿#include <iostream>
#include "BellmanFord.h"
#include <chrono>

std::string helpSwitch = "--help";
std::string help = "This console appliaction takes 0 arguments (--help is an exception)."
"After starting the application you will be asked to enter number of edges and the number of vertices."
"Then you will be asked to enter every edge, an edge in this case is a triplet -  \"source destination weight\""
", where source and destination means vertices represented by an int, weight is value of edge, must fit into int."
"Finally the input source vertex is an int between 0 and number of vertices - 1";
template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

void printHelp() {
    std::cout <<std::endl<< help << std::endl;
}
int main(int argc, char* argv[])
{
    if (argc > 2) {
        std::cerr << "Too many arguments" << std::endl;
        printHelp();
        return 1;
    }

    if (argc == 2) {
        if (argv[1] == helpSwitch) {
            printHelp();
            return 0;
        }
        else {
            std::cerr << "This argument is not supported" << std::endl;
            printHelp();
            return 1;
        }
    }
    try {
        std::cout << "Input number of vertices" <<std::endl;
        int vertices;
        int numEdges;
        std::cin >> vertices;
        if (std::cin.fail()) {
            throw std::invalid_argument("Must be int");
        }
        std::cout << "Input number of edges" << std::endl;
        std::cin >> numEdges;
        if (std::cin.fail()) {
            throw std::invalid_argument("Must be int");
        }
        std::vector<Graph::Edge> edges;
        for (size_t i = 0; i < numEdges; i++)
        {
            int src, dest, wght;
            std::cin >> src >> dest >> wght;
            if (std::cin.fail()) {
                throw std::invalid_argument("All three arguments must be int");
            }
            if (src >= numEdges || src < 0 || dest < 0 || dest>=numEdges) {
                throw std::invalid_argument("Source and destination must be smaller than number of verices and positive");
            }
            Graph::Edge edge(src, dest, wght);
            edges.push_back(edge);
        }
        std::cout << "Finally, give me the source vertex!" <<std::endl;
        int source;
        std::cin >> source;
        if (std::cin.fail()) {
            throw std::invalid_argument("Source must be int");
        }
        if (source >= vertices || source < 0) {
            throw std::invalid_argument("Source must be smaller than number of vertices and positive");
        }
        auto start = std::chrono::high_resolution_clock::now();
        Graph* graph = new Graph(edges, vertices);
        graph->BellmanFord(std::cout, source);
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "Needed " << to_ms(end - start).count() << " ms to finish.\n";
        delete graph;
        return 0;
    }
    catch (std::invalid_argument ex) {
        std::cerr << "incorrect input: " << ex.what();
        printHelp();
        return 1;
    }
}