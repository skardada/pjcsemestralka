#include "BellmanFord.h"
#include <climits>


void Graph::BellmanFord(std::ostream &os, int source) {
	int* distances = new int[this->numVertices];
	for (size_t i = 0; i < this->numVertices; i++)
	{
		distances[i] = INT_MAX;
	}
	distances[source] = 0;
	for (size_t i = 0; i < this->numVertices; i++)
	{
		for (size_t j = 0; j < this->edges.size(); j++)
		{
			if (distances[this->edges[j].source] != INT_MAX &&
				distances[this->edges[j].source] + this->edges[j].weight < distances[this->edges[j].dest]) {
				distances[this->edges[j].dest] = distances[this->edges[j].source] + this->edges[j].weight;
			}
		}
	}
	for (size_t j = 0; j < this->edges.size(); j++)
	{
		if (distances[this->edges[j].source] != INT_MAX &&
			distances[this->edges[j].source] + this->edges[j].weight < distances[this->edges[j].dest]) {
			os << "Negative Cycle present!" << std::endl;
			return;
		}
	}
	os << "Distances from source: " << source<< std::endl;
	os << "Vertex  |  " << "Distance" << std::endl;
	for (size_t i = 0; i < this->numVertices; i++)
	{
		if (distances[i] == INT_MAX) {
			os << i << "  |  " << "not reachable" << std::endl;
		}
		else {
			os << i << "  |  " << distances[i] << std::endl;
		}
	}
	delete[] distances;
}