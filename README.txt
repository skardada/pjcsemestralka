Zadání:
	-Nejkratší cesty mezi všemi vrcholy v grafu 
	-Graf je orientovaný, hrany můžou mít negativní hodnotu
Použité řešení:
	-Bellman-Ford algoritmus
	-jedno vlákno (více vláknové řešení je zbytečně složité)
	-přijímá vstupy z příkazové řádky
		--help funguje
		-nejdříve počet vrcholů a hran, poté jednotlivé hrany a jejich hodnoty, nakonec zdrojový vrchol
		-ukázkové vstupy jsou v EXEMPLARY_INPUT.txt	
Měření času:
	-použité vstupy v EXEMPLARY_INPUT.txt
	-měření ve Wsl 	
	-input 1 - 10 měření - median 8ms (občas je výkyv 1 ms)
	-input 3 - 10 měření - median 1ms (zhruba 50/50 1ms a 2ms)
	-většinu času zabírá vypisování a ne samotný algoritmus (důkaz je rozdíl mezi 1 a 2 měřením)
